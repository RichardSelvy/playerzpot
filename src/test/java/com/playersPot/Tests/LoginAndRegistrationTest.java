package com.playersPot.Tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import static org.testng.Assert.assertEquals;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Unit test for simple App.
 */
public class LoginAndRegistrationTest 
{

	WebDriver driver;
	WebDriverWait wait;
	WebElement element;
	Random random ;
	StringBuilder sb;
	StringBuilder sb2;

	@BeforeTest
	public void setUp() {

		WebDriverManager.chromedriver().setup();
		random = new Random();
		String id = String.format("%04d", random.nextInt(10000));
		sb = new StringBuilder("949301");
		sb.append(id);

		sb2 = new StringBuilder(id);
		sb2.append("@mailinator.com");

	}

	@BeforeMethod
	public void launchBrowser()
	{
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();	
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 10);


		driver.get("https://qcweb.playerzpot.com/");
	}


	@Test(enabled= true, priority=0)
	public void register() 
	{ 
		boolean status = false;

		status = driver.getTitle().equalsIgnoreCase("Playerzpot Login - Playerzpot.com");
		AssertJUnit.assertTrue(status);
		System.out.println("User is on the Playerzpot Login - Playerzpot.com screen");

		driver.findElement(By.linkText("Sign Up")).click();
		System.out.println("Tapped on the signUp button");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[text() = 'Enter Mobile Number']/following-sibling::input")));
		element = driver.findElement(By.xpath("//label[text() = 'Enter Mobile Number']/following-sibling::input"));
		element.clear();

		//generating random phone number and emailid

		element.sendKeys(sb);
		System.out.println("Entered mobile number on SignUp screen");

		element = driver.findElement(By.xpath("//label[text() = 'Enter Email ID']/following-sibling::input"));
		element.clear();
		element.sendKeys(sb2);
		System.out.println("Entered EmailId on SignUp screen");

		driver.findElement(By.xpath("//span[contains(text(),'NEXT')]")).click();

		driver.findElement(By.xpath("//button[text()='OK']")).click();

		driver.findElement(By.xpath("//label[text()='Password']/following-sibling::input")).sendKeys("12345678");
		System.out.println("Entered Password on SignUp screen");

		element = driver.findElement(By.tagName("select"));
		Select s = new Select(element);
		s.selectByValue("22");
		System.out.println("Selected Maharastra from the DropDown list");

		driver.findElement(By.xpath("//button[text()='Register']")).click();
		System.out.println("Tapped on Register Button");

		wait.until(ExpectedConditions.textToBe(By.xpath("//div[text()='Enter OTP']"), "Enter OTP"));
		driver.findElement(By.xpath("//u[text()='Change Number']/parent::div/following-sibling::form/div/input")).sendKeys("123456");
		System.out.println("Enters the OTP");

		driver.findElement(By.xpath("//button[text()='Confirm & Get Registered']")).submit();
		System.out.println("Tapped on Confirm and Register");

		System.out.println("User created an Account successfully");
	}


	@Test(enabled=true,priority = 1)
	public void addMoney()
	{
		//Login

		boolean status = false;

		status = driver.getTitle().equalsIgnoreCase("Playerzpot Login - Playerzpot.com");
		AssertJUnit.assertTrue(status);
		System.out.println("User is on the Playerzpot Login - Playerzpot.com screen");


		driver.findElement(By.xpath("//label[text() = 'Enter mobile number']/following-sibling::input")).sendKeys(sb);
		System.out.println("Entered mobile number on Login screen");


		driver.findElement(By.xpath("//form/div[3]/input")).click();
		System.out.println("Tapped on Login Button");

		try{
			//to enter password
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//label[text()='Password']/following-sibling::input")));
			driver.findElement(By.xpath("//label[text()='Password']/following-sibling::input")).sendKeys("12345678");
			driver.findElement(By.xpath("//input[@class='ppm_btn_blue log_bt']")).click();
			System.out.println("Entered Password on Login screen");
		}
		catch(Exception e){
			//to enter Otp
			driver.findElement(By.xpath("//form/div/input")).sendKeys("123456");
			System.out.println("Entered OTP on Login screen");

			driver.findElement(By.xpath("//button[text()='Login']")).click();
			System.out.println("Tapped on Login Button after entring the OTP");

		}
		System.out.println("Logged In Successfully");

		//AddMoney

		driver.findElement(By.xpath("//div/a/div[text()='MORE']/preceding-sibling::img")).click();
		System.out.println("Tapped on more options from the Menu");

		driver.findElement(By.xpath("//div[contains(text(),'WALLET')]")).click();
		System.out.println("Tapped on Wallet Option");

		driver.findElement(By.xpath("//button[text()='Deposit']")).click();
		System.out.println("Tapped on Deposit Button on Wallet screen");

		driver.findElement(By.xpath("//div[contains(text(),'Add money')]/parent::div/preceding-sibling::div/input")).sendKeys("525");
		System.out.println("Tapped on Deposit Button on Wallet screen");

		driver.findElement(By.xpath("//div[contains(text(),'Add money')]")).click();
		System.out.println("Tapped on Add money Button on Wallet screen");

		driver.findElement(By.xpath("//div[contains(text(),'Add new card')]")).click();
		System.out.println("Selected Add new Card option from Paymemt Options Screen");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("payu_logo")));
		System.out.println("Navigated to Payment PayU Portal");

		driver.findElement(By.xpath("//label[contains(text(),'Card Number')]/following-sibling::input")).sendKeys("5123");// 4567 8901 2346
		driver.findElement(By.xpath("//label[contains(text(),'Card Number')]/following-sibling::input")).sendKeys("4567");
		driver.findElement(By.xpath("//label[contains(text(),'Card Number')]/following-sibling::input")).sendKeys("8901");
		driver.findElement(By.xpath("//label[contains(text(),'Card Number')]/following-sibling::input")).sendKeys("2346");
		System.out.println("Entered the Card Number");

		driver.findElement(By.xpath("//label[contains(text(),'Name')]/following-sibling::input")).sendKeys("Rick");
		System.out.println("Entered the Name on Card");

		driver.findElement(By.xpath("//label[contains(text(),'CVV')]/following-sibling::input")).sendKeys("123");
		System.out.println("Entered the CVV");

		element = driver.findElement(By.id("cexpiry_date_month"));
		Select s2= new Select(element);
		s2.selectByValue("08");
		System.out.println("Selected Expiry Month");

		element = driver.findElement(By.id("cexpiry_date_year"));
		s2= new Select(element);
		s2.selectByValue("2023");
		System.out.println("Selected Expiry Year");


		driver.findElement(By.id("pay_button")).click();
		System.out.println("Tapped on Pay Button");


		// Otp screen

		driver.findElement(By.id("password")).sendKeys("123456");
		System.out.println("Entered the OTP");


		driver.findElement(By.id("submitBtn")).click();
		System.out.println("Tapped on Submit Button");


		//Logout

		driver.navigate().to("https://qcweb.playerzpot.com/menu");
		System.out.println("Naviagted back to More Options screen");

		driver.findElement(By.xpath("//div[contains(text(),'LOGOUT')][1]")).click();
		driver.findElement(By.xpath("//a/div/div[contains(text(),'LOGOUT')][1]")).click();
		System.out.println("Logged Out of the Account Successfully");





	}


	@AfterMethod
	public void tearDown()
	{
//		driver.quit();		
	}



}
